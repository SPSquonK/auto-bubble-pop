const robotjs = require('robotjs');
const readline = require('readline');

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function askQuestion(query) {
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  return new Promise(resolve => rl.question(query, ans => {
    rl.close();
    resolve(ans);
  }))
}

class MinMaxSearcher {
  constructor() {
    this.min = null; this.max = null;
  }

  add(value) {
    if (this.min == null || this.min > value) this.min = value;
    if (this.max == null || this.max < value) this.max = value;
  }
}

function findReferenceColors(topLeft, bottomRight) {
  let result = new Set();
  const width = bottomRight.x - topLeft.x;
  const height = bottomRight.y - topLeft.y;

  const screen = robotjs.screen.capture(topLeft.x, topLeft.y, width, height);

  for (let x = 0; x != width; ++x) {
    for (let y = 0; y != height; ++y) {
      result.add(screen.colorAt(x, y));
    }
  }

  return [...result];
}

async function main() {

  await askQuestion("Put mouse on on the top-left of the area");
  const topLeft = robotjs.getMousePos();

  await askQuestion("Put mouse on on the bottom-right of the area");
  const bottomRight = robotjs.getMousePos();
  

  if (topLeft.x >= bottomRight.x) return;
  if (topLeft.y >= bottomRight.y) return;


  let referenceColors;
  
  while (true) {
    referenceColors = findReferenceColors(topLeft, bottomRight);
    console.log(`Found ${referenceColors.length} colors`);

    if (referenceColors.length == 2) break;
    console.log("Rerolling");
    await sleep(500);
  }
  
  console.log("Starting bubble pop");
  const width = bottomRight.x - topLeft.x;
  const height = bottomRight.y - topLeft.y;

  while (true) {
    // Scan
    const screen = robotjs.screen.capture(topLeft.x, topLeft.y, width, height);

    let xs = new MinMaxSearcher();
    let ys = new MinMaxSearcher();
    let numberInReference = 0;

    for (let x = 0; x < width; x += 10) {
      for (let y = 0; y < height; y += 10) {
        const col = screen.colorAt(x, y);
        if (referenceColors.includes(col)) {
          ++numberInReference;
        } else {
          xs.add(x);
          ys.add(y);
        }
      }
    }

    let point = null;
    if (xs.min != null) {
      point = {
        x: topLeft.x + xs.min + (xs.max - xs.min) / 2,
        y: topLeft.y + ys.min + (ys.max - ys.min) / 2
      };
    }

    // Click
    if (point != null) {
      if (numberInReference < width * height / 400) {
        console.log("Waiting, probably alt tabbed");
      } else {
        robotjs.moveMouse(point.x, point.y);
        await sleep(10);
        robotjs.mouseClick("left");
        await sleep(2000);
      }
    }

    // Sleep
    await sleep(300);
  }
}

main();
