# Auto Bubble Pop

A software to automatically click on bubbles in Idle Bouncer.

## How to install

Clone the repository, `npm install`

## How to use

- `node index.js`
- Put the balls at a low height
- Tell the program to scan the area from just below the top of the tubes, and to just above the balls
- Wait for 10 hours

